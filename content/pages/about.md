---
title: about
comments: false
---

## look at this beauty

**_En fyrir því at Óðinn var forspár ok fjǫlkunnigr, þá vissi hann, at hans afkvæmi mundi um norðrhálfu heimsins byggja._**

'So whereas Odin was foreseeing, and wise in wizardry, he knew that his offspring should people the Northern Parts of the World.'

## who **greekfellows** is

**greekfellows** is

*	a terrible `c++11`'er
* 	a linguistics<sub>i</sub> student interested in its<sub>i</sub> formalism and philosophy

## whereto herefrom

*	his <a href="https://github.com/greekfellows">github</a>
*	his <a href="https://gitlab.com/greekfellows">gitlab</a>
