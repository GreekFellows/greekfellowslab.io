---
title: "html gloss"
date: 2017-12-21T15:52:20+08:00
categories: ["html", "sprak"]
---

## what?

an ongoing attempt to do linewrapping interlinear gloss in HTML, probably using flex-boxes. see [here](gloss.html).

## glossor, a translator

I could write a program that takes a file, identifies the following structure:

```
gloss {
	wita-jarra-rlu	ka-pala			wajili-pi-nyi	yalumpu		kurdu-jarra-rlu	maliki
	small-DUAL-ERG	pres-3.DU.SUBJ	chase-NPAST		that.ABS	child-DUAL-ERG	dog.ABS
	'The two small children are chasing that dog.'
}
```

and turns that into some HTML like

```html
<!--glossed {
	wita-jarra-rlu	ka-pala			wajili-pi-nyi	yalumpu		kurdu-jarra-rlu	maliki
	small-DUAL-ERG	pres-3.DU.SUBJ	chase-NPAST		that.ABS	child-DUAL-ERG	dog.ABS
	'The two small children are chasing that dog.'
}-->
<div class="gloss" style="display: inline-grid;">
	<div class="gloss-cols" style="display: flex; flex-direction: row; flex-wrap: wrap;">
		<div class="gloss-col" style="padding: 10px 10px 0px 0px;">
			<span class="gloss-col-word" style="font-weight: bold; font-style: italic;">wita-jarra-rlu</span>
			<br />
			<span class="gloss-col-gloss">small-DUAL-ERG</span>
		</div>
		<div class="gloss-col" style="padding: 10px 10px 0px 0px;">
			<span class="gloss-col-word" style="font-weight: bold; font-style: italic;">ka-pala</span>
			<br />
			<span class="gloss-col-gloss">pres-3.DU.SUBJ</span>
		</div>
		<div class="gloss-col" style="padding: 10px 10px 0px 0px;">
			<span class="gloss-col-word" style="font-weight: bold; font-style: italic;">wajili-pi-nyi</span>
			<br />
			<span class="gloss-col-gloss">chase-NPAST</span>
		</div>
		<div class="gloss-col" style="padding: 10px 10px 0px 0px;">
			<span class="gloss-col-word" style="font-weight: bold; font-style: italic;">yalumpu</span>
			<br />
			<span class="gloss-col-gloss">that.ABS</span>
		</div>
		<div class="gloss-col" style="padding: 10px 10px 0px 0px;">
			<span class="gloss-col-word" style="font-weight: bold; font-style: italic;">kurdu-jarra-rlu</span>
			<br />
			<span class="gloss-col-gloss">child-DUAL-ERG</span>
		</div>
		<div class="gloss-col" style="padding: 10px 10px 0px 0px;">
			<span class="gloss-col-word" style="font-weight: bold; font-style: italic;">maliki</span>
			<br />
			<span class="gloss-col-gloss">dog.ABS</span>
		</div>
	</div>
	<div class="gloss-trans" style="padding: 10px 10px 0px 0px;">
		<span>'The two small children are chasing that dog.'</span>
	</div>
</div>
```

which looks like:

<div class="gloss" style="display: inline-grid;">
	<div class="gloss-cols" style="display: flex; flex-direction: row; flex-wrap: wrap;">
		<div class="gloss-col" style="padding: 10px 10px 0px 0px;">
			<span class="gloss-col-word" style="font-weight: bold; font-style: italic;">wita-jarra-rlu</span>
			<br />
			<span class="gloss-col-gloss">small-DUAL-ERG</span>
		</div>
		<div class="gloss-col" style="padding: 10px 10px 0px 0px;">
			<span class="gloss-col-word" style="font-weight: bold; font-style: italic;">ka-pala</span>
			<br />
			<span class="gloss-col-gloss">pres-3.DU.SUBJ</span>
		</div>
		<div class="gloss-col" style="padding: 10px 10px 0px 0px;">
			<span class="gloss-col-word" style="font-weight: bold; font-style: italic;">wajili-pi-nyi</span>
			<br />
			<span class="gloss-col-gloss">chase-NPAST</span>
		</div>
		<div class="gloss-col" style="padding: 10px 10px 0px 0px;">
			<span class="gloss-col-word" style="font-weight: bold; font-style: italic;">yalumpu</span>
			<br />
			<span class="gloss-col-gloss">that.ABS</span>
		</div>
		<div class="gloss-col" style="padding: 10px 10px 0px 0px;">
			<span class="gloss-col-word" style="font-weight: bold; font-style: italic;">kurdu-jarra-rlu</span>
			<br />
			<span class="gloss-col-gloss">child-DUAL-ERG</span>
		</div>
		<div class="gloss-col" style="padding: 10px 10px 0px 0px;">
			<span class="gloss-col-word" style="font-weight: bold; font-style: italic;">maliki</span>
			<br />
			<span class="gloss-col-gloss">dog.ABS</span>
		</div>
	</div>
	<div class="gloss-trans" style="padding: 10px 10px 0px 0px;">
		<span>'The two small children are chasing that dog.'</span>
	</div>
</div>

Works for anything that builds into HTML, such as Markdown.

A few important things:

*	Do not generate any empty HTML lines, otherwise embedding a gloss in lists or potentially other environments seem to mess up alignment and indentations.
* 	Enclose the original input in comments and change `gloss` to `glossed`.
* 	There should be a symbol that represents an empty word or gloss in a gloss column.
* 	Gloss columns should be separated by tab characters.
