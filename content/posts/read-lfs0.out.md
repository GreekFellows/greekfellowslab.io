---
title: "read-lfs0"
date: 2017-12-21T14:56:42+08:00
categories: ["read", "lfs"]
# draft: true
---

Bresnan, Joan.  _Lexical-Functional Syntax_. Malden: Blackwell Publishers, 2001.

## intro

本書は、lexical-functional grammar を紹介するという内容であり、part 1 は LFG に至るまでの motivation を、generative grammar と LFG の比較を通して説明している。

## 1. nonconfigurationality

nonconfigurational な言語とは、discontinuous な constituent がありえる言語のことである。例として、同じ意味である英語の文 (1) と Warlpiri の (2) を比較してみる。

1.	The two small children are chasing that dog.
2.	<!--gloss {
		wita-jarra-rlu	ka-pala			wajili-pi-nyi	yalumpu		kurdu-jarra-rlu	maliki
		small-DUAL-ERG	pres-3.DU.SUBJ	chase-NPAST		that.ABS	child-DUAL-ERG	dog.ABS
		'The two small children are chasing that dog.'
}--><div class="gloss" style="display: inline-grid;">
	<div class="gloss-cols" style="display: flex; flex-direction: row; flex-wrap: wrap;">
		<div class="gloss-col" style="padding: 10px 10px 0px 0px;">
			<span class="gloss-col-word" style="font-weight: bold; font-style: italic;">wita-jarra-rlu</span>
			<br />
			<span class="gloss-col-gloss">small-DUAL-ERG</span>
		</div>
		<div class="gloss-col" style="padding: 10px 10px 0px 0px;">
			<span class="gloss-col-word" style="font-weight: bold; font-style: italic;">ka-pala</span>
			<br />
			<span class="gloss-col-gloss">pres-3.DU.SUBJ</span>
		</div>
		<div class="gloss-col" style="padding: 10px 10px 0px 0px;">
			<span class="gloss-col-word" style="font-weight: bold; font-style: italic;">wajili-pi-nyi</span>
			<br />
			<span class="gloss-col-gloss">chase-NPAST</span>
		</div>
		<div class="gloss-col" style="padding: 10px 10px 0px 0px;">
			<span class="gloss-col-word" style="font-weight: bold; font-style: italic;">yalumpu</span>
			<br />
			<span class="gloss-col-gloss">that.ABS</span>
		</div>
		<div class="gloss-col" style="padding: 10px 10px 0px 0px;">
			<span class="gloss-col-word" style="font-weight: bold; font-style: italic;">kurdu-jarra-rlu</span>
			<br />
			<span class="gloss-col-gloss">child-DUAL-ERG</span>
		</div>
		<div class="gloss-col" style="padding: 10px 10px 0px 0px;">
			<span class="gloss-col-word" style="font-weight: bold; font-style: italic;">maliki</span>
			<br />
			<span class="gloss-col-gloss">dog.ABS</span>
		</div>
	</div>
	<div class="gloss-trans" style="padding: 10px 10px 0px 0px;">
		<span>'The two small children are chasing that dog.'</span>
	</div>
</div>

Bresnan 曰く、英語では、conceptual unit は continuous であるのに対し、Warlpiri では discontinuous でも問題はない。この conceptual unit は動詞の argument のことなのか、subject や object 等の grammatical relations なのかはよくわからないが、とにかく Bresnan が言いたいのは、**言語によっては phrase structure が英語の様に rigid なものもあれば、Warlpiri の様に non-rigid なのもある**、ということだ。

如何なる grammar でも、subject や object 等の grammatical relations に対する定義がなければいけない。Chomsky が提唱する generative grammar では、subject は S の下の、VP の姉妹にあたる NP、object は VP の下の、V の姉妹に当たる NP という風に定義されるのだが、これは英語などの rigid phrase structure を有する言語の影響によるものであって、Warlpiri の様な non-rigid phrase structure を持つ言語にこのような定義を適用するためには、**Warlpiri の一見 non-rigid な phrase structure が surface structure なのであって、deep structure では英語のような rigid phrase structure が存在するという仮定をしなければならない。**

しかし、その仮定は果たして正しいのだろうか？Warlpiri の様な言語には VP という概念すら存在しないという説を、Van Valin による _An Introduction to Syntax_ で読んだ覚えがある。本書の part 4 では、reflexive pronouns に対する constraint が、phrase structure ではなく predication relation に影響されているという説を支持する内容があるらしい。

generative grammar に対して、LFG では、grammatical functions が phrase structure configurations に reduce されることはできない。subject や object は grammatical function であり、argument structure と expression structure に接続し、両者を繋げる役目を果たす。

ここで Bresnan は、phrase structure を基に作られた grammar を configurational design といい、LFG を relational design という。configurational design は NP や VP 等の phrase structure の要素が abstract syntactic functions と semantic predicate argument structures の根底に存在するということを意味し、relational design は predicators や arguments や grammatical functions が一番底にあるということを意味する。ここから Bresnan は、configurational design では説明できないが relational design では説明できる現象を例として挙げることになる。

## 1.1 predicate argument structure

Marantz (1984) によれば、grammatical function を NP や VP 等の phrase structure で定義する場合、subject と object は asymmetric depth、つまり木構造にした時の深さの違いがあるので、両者には他の方面においてでも asymmetry があるはず、と言っている。reflexive pronoun が subject を指示することはできるが、object を指示することはできないのが、その asymmetry の一つの例とされている。

ここで Bresnan は、idiom に関するもう一つの例を挙げる。動詞は object と idiom を作ることが、subject と idiom を作るより遥かに多いということだ。Kiparsky (1987) と O'Grady (1998) によれば、動詞と idiom を作る NP の semantic role には (3) の様な作りやすさの hierarchy があるという。

3.	agent > beneficiary > recipient > ... > patient/theme > location

動詞と locative が一番多く、次に patient/theme、一番少ないのが動詞と agent ということになる。一見動詞と subject が idiom を作ってる場合も、実は subject が theme だったりするということ。英語で動詞と locative が idiom を作る例がこちら。

4.	Mary **put** John **to shame**.
5.	Mary's innocent look **took** John **in**.
6.	This office has **gone to the dogs**!

**continuity, substitutability, reorderings と phonological phrasing を重んじる configurational design にはこの idiom の作りやすさが説明できない。何故なら、locative と動詞の間には object NP があるので、連続した constituent を形成できないからだ。**

アメリカ大陸、アフリカやオセアニアの言語でも、この hierarchy が発見されているらしい。そのうち、syntactically ergative な言語では、transitive な動詞の patient は subject である。hierarchy を見る限り、patient は idiom になりやすい NP なのだが、configurational design では subject は動詞と constituent をなさないので、idiom になりにくい NP であるはず。しかし、syntactically ergative な言語でも、patient が idiom になるというの問題のないことである。Dyirbal の例もいくつか上げられている。
