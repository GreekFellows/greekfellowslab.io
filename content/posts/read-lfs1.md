---
title: "read-lfs1"
date: 2017-12-26T22:23:07+08:00
# draft: true
---

Bresnan, Joan.  _Lexical-Functional Syntax_. Malden: Blackwell Publishers, 2001.

## 2. movement paradoxes

nonconfigurationality の様な typology により motivate される LFG は、かえって英語などの configurational な言語における configurational な現象をどう説明できるのだろうか？例として、本章では extraction configuration を取り上げている。(1) のような文は、ある constituent が元ある位置から移動した結果であるという考え方である。移動してもしなくても、この constituent が prepositional object という grammatical function を持っていることは変わらない。

1.	[That problem] we talked about _ for days.

このような現象は、syntactical transformations、つまり structure-dependent な操作によって grammatical function を示す deep structure を surface structure に変換する原理が働いていることを裏付けしているように見える、と Bresnan はいう。そして英語でこのような原理が存在するならば、他の言語でも同じ原理が存在するはずなので、configurational design が結果として生まれる。

しかし、(2) のような文を movement として説明できない。constituent が元ある位置から移動したと考えると、constituent をその元ある位置においては文が ungrammatical になってしまう。

2.	(a) が movement 後、(b) が movement 前、\(c) が正しい
	a.	[That languages are learnable] is captured _ by this theory.
	b.	\*This theory captures [that languages are learnable].
	c.	This theory captures [the fact that languages are learnable].

このような問題を、Bresnan は category mismatch と呼んでいる。

## 2.1 theoretical assumptions
